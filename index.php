<head>
    <title>Valuta omregner</title>
    <link href="design/index.css" rel="stylesheet">
    <script src="design/scripts.js"></script>
</head>

<header>

    <p class="headertitle">Valuta omregner</p>
</header>


<div class="box">

    <p class="inputtitle">Valuta:</p>

    <script>getLanguages()</script>
    <select class="inputselectbox" id="languages1">

    </select>

    <p class="inputtitle">Antal:</p>
    <input type="text" id="amount" class="inputtextbox">

    <div class="switch" onclick="switchFields()">Switch</div>

    <p class="inputtitle">Valuta:</p>

    <select class="inputselectbox" id="languages2">

    </select>

    <!---
    <p class="inputtitle">Antal af den valgte valuta:</p>
    <input type="text" id="amountselected" class="inputtextbox">
    --->

    <button onclick="getResult()">Udregn</button>

    <div class="result">

        <p class="resulttext" id="resulttext">0</p>

    </div>

</div>