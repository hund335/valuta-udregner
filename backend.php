<?php

$url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
$c = curl_init();
$headers = ['Content-Type: application/xml; charset=utf-8'];
curl_setopt_array($c, array(CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0, CURLOPT_URL => $url, CURLOPT_HTTPHEADER => $headers, CURLOPT_RETURNTRANSFER => 1, CURLOPT_CUSTOMREQUEST => "GET"));
$result = curl_exec($c);

curl_close($c);


$xml = simplexml_load_string($result);
$json = json_encode($xml);

$array = json_decode($json, true);


$langs = array();

foreach ($array["Cube"]["Cube"]["Cube"] as $key) {

    array_push($langs, array("currency" => $key["@attributes"]["currency"], "rate" => $key["@attributes"]["rate"]));

}

$jsonarray = array("Languages" => $langs);
echo json_encode($jsonarray);
