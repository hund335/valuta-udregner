function getLanguages() {
    var req = new XMLHttpRequest();

    req.onreadystatechange = function () {
        if (req.readyState == XMLHttpRequest.DONE) {
            let languages1 = document.getElementById('languages1');
            let languages2 = document.getElementById('languages2');

            let json = JSON.parse(req.responseText);

            for(let i = 0; i < json.Languages.length; i++){

                var option = document.createElement("option");
                option.innerText = json.Languages[i].currency;
                option.setAttribute("value", json.Languages[i].rate);

                languages1.appendChild(option);
            }

            for(let i = 0; i < json.Languages.length; i++){

                var option = document.createElement("option");
                option.innerText = json.Languages[i].currency;
                option.setAttribute("value", json.Languages[i].rate);

                languages2.appendChild(option);
            }
        }
}
req.open('GET', '../backend.php', true);
req.send();
}

function switchFields() {

    let language1 = document.getElementById('languages1');
    let language2 = document.getElementById('languages2');

    let oldval1 = language1.value;
    let oldval2 = language2.value;

    language1.value = oldval2;
    language2.value = oldval1;

    getResult();


}

function getResult() {


    let language1 = document.getElementById('languages1');
    let amount = document.getElementById('amount');
    let language2 = document.getElementById('languages2');



    document.getElementById('resulttext').innerText = parseFloat((language2.value / language1.value) * amount.value).toFixed(2);;



}